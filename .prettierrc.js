module.exports = {
  "useTabs": false,
  "semi": true,
  "tabWidth": 2,
  "singleQuote": true,
  "bracketSpacing": true,
  "trailingComma": "es5",
  "printWidth": 120,
  "endOfLine":"auto"
};
