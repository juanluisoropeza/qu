import React from 'react';
import PropTypes from 'prop-types';

const People = ({ people }) => {
  return (
    <>
      <h2>People</h2>
      <div className="row">
        {people.map((person) => (
          <div className="col-md-4 mb-2" key={person.created}>
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-3">{person.name}</h4>
                <div className="content mb-3">
                  <div className="card-text">
                    <b>Height: </b>
                    {person.height}
                  </div>
                  <div className="card-text">
                    <b>Mass: </b>
                    {person.mass}
                  </div>
                  <div className="card-text">
                    <b>Hair: </b>
                    {person.hair_color}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

People.propTypes = {
  people: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default People;
