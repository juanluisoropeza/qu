/* eslint-disable no-await-in-loop */
/* eslint-disable react/no-array-index-key */
/* eslint-disable array-callback-return */
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';

const Home = ({ films }) => {
  const [listCharacters, setListCharacters] = useState([]);
  useEffect(() => {
    const refactoringData = async () => {
      const data = [];
      for (let i = 0; i < films.length; i++) {
        const element = films[i];
        const filmInfo = {
          title: element.title,
          characters: [],
        };
        for (let j = 0; j < element.characters.length; j++) {
          const item = element.characters[j];
          const result = await axios.get(item);
          filmInfo.characters.push(result.data.name);
        }
        data.push(filmInfo);
      }
      setListCharacters(data);
    };
    refactoringData();
  }, []);
  return (
    <>
      <div className="row mt-4">
        {listCharacters.length === 0 ? (
          <div className="col-lg-12 text-center">
            <p>Please, wait a moment while is colected the info.</p>
            <p>(it may take a while.)</p>
            <FontAwesomeIcon className="spinner" icon={faSpinner} />
          </div>
        ) : (
          listCharacters.map((film) => (
            <div className="col-md-3 mb-3" key={film.title}>
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title mb-3">{film.title}</h5>
                  <div className="content mb-3">
                    <div className="card-text">
                      <small>
                        <b>Episode: </b>
                        {film.episode_id}
                      </small>
                    </div>
                    <div className="card-text">
                      <small>
                        <b>Director: </b>
                        {film.director}
                      </small>
                    </div>
                    <div className="card-text">
                      <small>
                        <b>Release Date: </b>
                        {moment(film.release_date).format('L')}
                      </small>
                    </div>
                    <div className="card-text">
                      <p>
                        <b>Characters: </b>
                        {film.characters.map((character, i) => (
                          <small className="character" key={i}>
                            {character}
                          </small>
                        ))}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))
        )}
      </div>
    </>
  );
};

Home.propTypes = {
  films: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Home;
