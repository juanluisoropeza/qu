import React from 'react';
import PropTypes from 'prop-types';
import utils from '../helpers/utils';

const Planets = ({ planets }) => {
  return (
    <>
      <h2>Planets</h2>
      <div className="row">
        {planets.map((planet) => (
          <div className="col-md-4 mb-2" key={planet.name}>
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-3">{planet.name}</h4>
                <div className="content mb-3">
                  <div className="card-text">
                    <b>Terrain: </b> {planet.terrain}
                  </div>
                  <div className="card-text">
                    <b>Climate: </b> {planet.climate}
                  </div>
                  <div className="card-text">
                    <b>Population: </b>
                    {planet.population !== 'unknown' ? utils.formatNumbers(planet.population) : 'Without data'}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

Planets.propTypes = {
  planets: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Planets;
