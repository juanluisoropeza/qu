import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import Planets from './components/Planets';
import Navbar from './components/Navbar';
import Home from './components/Home';
import People from './components/People';

const App = () => {
  // principal states
  const [planets, setPlanets] = useState([]);
  const [people, setPeople] = useState([]);
  const [films, setFilms] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getPlanets = async () => {
      const url = 'https://swapi.dev/api/planets/';
      const result = await axios.get(url);
      setPlanets(result.data.results);
      setLoading(false);
    };
    const getPeople = async () => {
      const url = 'https://swapi.dev/api/people/';
      const result = await axios.get(url);
      setPeople(result.data.results);
      setLoading(false);
    };
    const getFilms = async () => {
      const url = 'https://swapi.dev/api/films/';
      const result = await axios.get(url);
      setFilms(result.data.results);
      setLoading(false);
    };
    getPlanets();
    getPeople();
    getFilms();
  }, []);

  return (
    <>
      <Router>
        <Navbar />
        <div className="container">
          {loading ? (
            <FontAwesomeIcon className="spinner" icon={faSpinner} />
          ) : (
            <Switch>
              <Route exact path="/">
                <Home films={films} />
              </Route>
              <Route exact path="/planets">
                <Planets planets={planets} />
              </Route>
              <Route exact path="/people">
                <People people={people} />
              </Route>
            </Switch>
          )}
        </div>
      </Router>
      {/* <div className="container">{planets.length > 0 ? <Planets planets={planets} /> : <div>No Hay registros</div>}</div> */}
    </>
  );
};

export default App;
